#ifndef KIO_7ZIP_TEST_UDS_ENTRY_HPP
#define KIO_7ZIP_TEST_UDS_ENTRY_HPP

#include <KIO/UDSEntry>

class QJsonArray;
class QJsonObject;

void
checkUDSEntry(KIO::UDSEntry const& entry, QJsonObject const&);

void
checkUDSEntries(KIO::UDSEntryList const& entries, QJsonArray const&);

#endif // KIO_7ZIP_TEST_UDS_ENTRY_HPP
