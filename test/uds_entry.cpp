#include "utility.hpp"

#include <algorithm>

#include <sys/stat.h>

#include <QDateTime>
#include <QDebug>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QString>

#include <KIO/UDSEntry>

#include <boost/test/unit_test.hpp>

static mode_t
getPermission(QJsonObject const& obj)
{
    auto const& s = obj["permission"].toString();
    bool ok = false;

    auto const result = s.toInt(&ok, 8);
    BOOST_REQUIRE(ok);

    return static_cast<mode_t>(result);
}

static QString
getName(KIO::UDSEntry const& entry)
{
    return entry.stringValue(KIO::UDSEntry::UDS_NAME);
}

static mode_t
getFileType(KIO::UDSEntry const& entry)
{
    return static_cast<mode_t>(
        entry.numberValue(KIO::UDSEntry::UDS_FILE_TYPE)
    );
}

static mode_t
getPermission(KIO::UDSEntry const& entry)
{
    return static_cast<mode_t>(
        entry.numberValue(KIO::UDSEntry::UDS_ACCESS)
    );
}

static QDateTime
getModificationTime(KIO::UDSEntry const& entry)
{
    return QDateTime::fromTime_t(
        static_cast<uint>(entry.numberValue(KIO::UDSEntry::UDS_MODIFICATION_TIME))
    );
}

static QDateTime
getModificationTime(QJsonObject const& obj)
{
    auto const& s = obj["date_time"].toString();

    return QDateTime::fromString(s, "yyyy-MM-dd HH:mm:ss");
}

static void
checkUDSCurrentDirectory(KIO::UDSEntry const& entry)
{
    BOOST_TEST(getName(entry) == ".");
    BOOST_TEST(getFileType(entry) == S_IFDIR);
    //BOOST_TEST(getModificationTime(entry) == "");
}

static KIO::UDSEntry const&
findEntry(KIO::UDSEntryList const& entries, QJsonObject const& obj)
{
    auto const& name = obj["name"].toString();

    auto const it = std::find_if(
        entries.begin(), entries.end(),
        [&](auto& entry) {
            return getName(entry) == name;
        });

    BOOST_TEST((it != entries.end()));

    return *it;
}

void
checkUDSEntry(KIO::UDSEntry const& entry, QJsonObject const& obj)
{
    BOOST_TEST(getName(entry) == obj["name"].toString());
    BOOST_TEST(getFileType(entry) == getPermission(obj));
    BOOST_TEST(getPermission(entry) == getPermission(obj));
    BOOST_TEST(getModificationTime(entry) == getModificationTime(obj));
}

void
checkUDSEntries(KIO::UDSEntryList const& entries, QJsonArray const& data)
{
    BOOST_REQUIRE(!entries.isEmpty());

    checkUDSCurrentDirectory(entries[0]);
    auto rest = entries;
    rest.pop_front();

    BOOST_REQUIRE(rest.size() == data.size());

    for (auto const& value: data) {
        auto const& obj = value.toObject();

        auto const& entry = findEntry(entries, obj);

        checkUDSEntry(entry, obj);
    }
}
