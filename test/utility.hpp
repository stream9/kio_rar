#ifndef KIO_RAR_TEST_UTILITY_HPP
#define KIO_RAR_TEST_UTILITY_HPP

#include <iostream>

#include <QDateTime>
#include <QDir>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonParseError>
#include <QString>
#include <QUrl>

#include <boost/test/unit_test.hpp>

inline std::ostream&
operator<<(std::ostream& os, QString const& s)
{
    return os << s.toUtf8().constData();
}

inline std::ostream&
operator<<(std::ostream& os, QDateTime const& d)
{
    return os << d.toString("yyyy-MM-dd HH:mm:ss");
}

inline QDir
dataDir()
{
    return { DATA_DIR };
}

inline QUrl
makeUrl(QString const& filename)
{
    QUrl url;
    auto const& path = dataDir().absoluteFilePath(filename);

    url.setScheme("rar");
    url.setPath(path);

    return url;
}

inline QJsonArray
makeJsonData(QByteArray const& json)
{
    QJsonParseError err;
    auto const& doc = QJsonDocument::fromJson(json, &err);

    BOOST_TEST_INFO(err.errorString());
    BOOST_REQUIRE(!doc.isNull());

    return doc.array();
}

#endif // KIO_RAR_TEST_UTILITY_HPP
