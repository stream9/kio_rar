#include "uds_entry.hpp"
#include "utility.hpp"

#include <boost/test/unit_test.hpp>

#include <QCryptographicHash>
#include <QDebug>
#include <QJsonArray>
#include <QJsonObject>
#include <QUrl>

#include <KIO/UDSEntry>
#include <KIO/TransferJob>

BOOST_AUTO_TEST_SUITE(kio_rar_)
BOOST_AUTO_TEST_SUITE(get_)

    struct Job
    {
        std::unique_ptr<KIO::TransferJob> job;

        int numChunk = 0;
        uint64_t totalSize = 0, processedSize = 0;
        QByteArray data;
        QString mimeType;

        void get(QUrl const& url)
        {
            job.reset(
                KIO::get(
                    url,
                    KIO::NoReload,
                    KIO::HideProgressInfo
                )
            );

            QObject::connect(job.get(), &KIO::TransferJob::totalSize,
                [&](KJob*, qulonglong const v) {
                    this->totalSize = v;
                });

            QObject::connect(job.get(), &KIO::TransferJob::processedSize,
                [&](KJob*, qulonglong const v) {
                    this->processedSize = v;
                });

            QObject::connect(job.get(),
                qOverload<KIO::Job*, QString const&>(&KIO::TransferJob::mimetype),
                [&](KIO::Job*, QString const& v) {
                    this->mimeType = v;
                });

            QObject::connect(job.get(), &KIO::TransferJob::data,
                [&](KIO::Job*, QByteArray const& buf) {
                    ++this->numChunk;
                    this->data.append(buf);
                });

            //BOOST_REQUIRE(job->exec());
            if (!job->exec()) {
                qDebug() << job->errorString();
            }

        }
    };

    void
    getAndCheck(QUrl const& url, QJsonObject const& obj)
    {
        Job job;
        job.get(url);

        auto const& content = obj["content"].toString();

        BOOST_TEST(job.numChunk > 0);
        BOOST_TEST(job.totalSize == content.size());
        BOOST_TEST(job.processedSize == content.size());
        BOOST_TEST(job.mimeType == "text/plain");
        BOOST_TEST(job.data == content);
    }

    BOOST_AUTO_TEST_CASE(ok1_)
    {
        auto const& data = makeJsonData(R"([
            {
                "type": "file",
                "name": "file1",
                "permission": "0100644",
                "content": "content1\n"
            },
            {
                "type": "directory",
                "name": "dir1",
                "permission": "040644",
                "files": [
                    {
                        "type": "file",
                        "name": "file2",
                        "permission": "0100644",
                        "content": "content2\n"
                    }
                ]
            }
        ])");

        QString const filename { "get1.rar" };

        auto const& url1 = makeUrl(filename + "/file1");
        auto const& obj1 = data[0].toObject();

        getAndCheck(url1, obj1);

        auto const& url2 = makeUrl(filename + "/dir1/file2");
        auto const& dir1 = (data[1].toObject())["files"].toArray();
        auto const& obj2 = dir1[0].toObject();

        getAndCheck(url2, obj2);
    }

    BOOST_AUTO_TEST_CASE(large_file_)
    {
        QString const filename { "get2.rar" };
        auto const& url = makeUrl(filename + "/file1");

        Job job;
        job.get(url);

        BOOST_TEST(job.numChunk > 0);
        BOOST_TEST(job.totalSize == 20 * 1000 * 1000);
        BOOST_TEST(job.processedSize == 20 * 1000 * 1000);
        BOOST_TEST(job.mimeType == "text/plain");

        QString const& hash =
            QCryptographicHash::hash(job.data, QCryptographicHash::Md5).toHex();
        BOOST_TEST(hash == "25f5ff844ee020bdaaee831121c60133");
    }

    BOOST_AUTO_TEST_CASE(symlink_)
    {
        QString const& filename { "symlink1.rar" };
        auto const& url = makeUrl(filename + "/file2");

        std::unique_ptr<KIO::TransferJob> job {
            KIO::get(url, KIO::NoReload, KIO::HideProgressInfo)
        };

        BOOST_REQUIRE(!job->exec());

        BOOST_TEST(job->error() == KIO::ERR_SLAVE_DEFINED);
    }

    BOOST_AUTO_TEST_CASE(error1_)
    {
        QString const filename { "get1x.rar" };
        auto const& url = makeUrl(filename + "/file1");

        std::unique_ptr<KIO::TransferJob> job {
            KIO::get(url, KIO::NoReload, KIO::HideProgressInfo)
        };

        BOOST_REQUIRE(!job->exec());

        BOOST_TEST(job->error() == KIO::ERR_DOES_NOT_EXIST);
    }

    BOOST_AUTO_TEST_CASE(error2_)
    {
        QString const filename { "get1.rar" };
        auto const& url = makeUrl(filename + "/file1x");

        std::unique_ptr<KIO::TransferJob> job {
            KIO::get(url, KIO::NoReload, KIO::HideProgressInfo)
        };

        BOOST_REQUIRE(!job->exec());

        BOOST_TEST(job->error() == KIO::ERR_DOES_NOT_EXIST);
    }

    BOOST_AUTO_TEST_CASE(error3_)
    {
        QString const filename { "get1.rar" };
        auto const& url = makeUrl(filename + "/dir1");

        std::unique_ptr<KIO::TransferJob> job {
            KIO::get(url, KIO::NoReload, KIO::HideProgressInfo)
        };

        BOOST_REQUIRE(!job->exec());

        BOOST_TEST(job->error() == KIO::ERR_IS_DIRECTORY);
    }

BOOST_AUTO_TEST_SUITE_END() // get_
BOOST_AUTO_TEST_SUITE_END() // kio_rar_
