#include "uds_entry.hpp"
#include "utility.hpp"

#include <memory>

#include <boost/test/unit_test.hpp>

#include <QDebug>
#include <QJsonArray>
#include <QJsonObject>
#include <QUrl>

#include <KIO/UDSEntry>
#include <KIO/ListJob>

BOOST_AUTO_TEST_SUITE(kio_rar_)
BOOST_AUTO_TEST_SUITE(listDir_)

    // execute listDir on <url> and compare result with <json>
    void
    listDirAndCheck(QUrl const& url, QJsonArray const& json)
    {
        std::unique_ptr<KIO::ListJob> job {
            KIO::listDir(url, KIO::HideProgressInfo)
        };

        KIO::UDSEntryList entries;

        QObject::connect(job.get(), &KIO::ListJob::entries,
            [&](KIO::Job*, KIO::UDSEntryList const& list) {
                entries = list;
            }
        );

        BOOST_REQUIRE(job->exec());

        checkUDSEntries(entries, json);
    }

    BOOST_AUTO_TEST_CASE(ok1_)
    {
        auto const& data = makeJsonData(R"([
            {
                "type": "file",
                "name": "file1",
                "permission": "0100644",
                "date_time": "2019-03-21 17:10:58"
            },
            {
                "type": "file",
                "name": "file2",
                "date_time": "2019-03-21 17:11:06",
                "permission": "0100666"
            }
        ])");

        QString const filename { "list_dir1.rar" };
        auto const& url = makeUrl(filename);

        listDirAndCheck(url, data);
    }

    BOOST_AUTO_TEST_CASE(ok2_)
    {
        auto const& data = makeJsonData(R"([
            {
                "type": "file",
                "name": "file1",
                "permission": "0100644",
                "date_time": "2019-03-21 17:19:44"
            },
            {
                "type": "file",
                "name": "file2",
                "permission": "0100644",
                "date_time": "2019-03-21 17:19:56"
            },
            {
                "type": "directory",
                "name": "dir1",
                "permission": "040755",
                "date_time": "2019-03-21 17:20:22",
                "files": [
                    {
                        "type": "file",
                        "name": "file3",
                        "permission": "0100644",
                        "date_time": "2019-03-21 17:20:12"
                    },
                    {
                        "type": "directory",
                        "name": "dir2",
                        "permission": "040755",
                        "date_time": "2019-03-21 17:20:46",
                        "files": [
                            {
                                "type": "file",
                                "name": "file4",
                                "permission": "0100644",
                                "date_time": "2019-03-21 17:20:46"
                            }
                        ]
                    }
                ]
            }
        ])");

        // root
        QString const filename { "list_dir2.rar" };
        auto const url = makeUrl(filename);

        listDirAndCheck(url, data);

        // dir1
        auto const& dir1 = data[2].toObject();
        auto const& data1 = dir1["files"].toArray();

        auto url1 = url;
        url1.setPath(url1.path() + "/dir1");

        listDirAndCheck(url1, data1);

        // dir2
        auto const& dir2 = data1[1].toObject();
        auto const& data2 = dir2["files"].toArray();

        auto url2 = url1;
        url2.setPath(url2.path() + "/dir2");

        listDirAndCheck(url2, data2);
    }

    BOOST_AUTO_TEST_CASE(error1_)
    {
        QString const filename { "list_dir1.rar" };
        auto const& url = makeUrl(filename + "/file1");

        std::unique_ptr<KIO::ListJob> job {
            KIO::listDir(url, KIO::HideProgressInfo)
        };

        BOOST_REQUIRE(!job->exec());

        BOOST_TEST(job->error() == KIO::ERR_IS_FILE);
    }

    BOOST_AUTO_TEST_CASE(error2_)
    {
        QString const filename { "list_dir1x.rar" };
        auto const& url = makeUrl(filename);

        std::unique_ptr<KIO::ListJob> job {
            KIO::listDir(url, KIO::HideProgressInfo)
        };

        BOOST_REQUIRE(!job->exec());

        BOOST_TEST(job->error() == KIO::ERR_CANNOT_ENTER_DIRECTORY);
    }

    BOOST_AUTO_TEST_CASE(error3_)
    {
        QString const filename { "list_dir1.rar" };
        auto const& url = makeUrl(filename + "/file3");

        std::unique_ptr<KIO::ListJob> job {
            KIO::listDir(url, KIO::HideProgressInfo)
        };

        BOOST_REQUIRE(!job->exec());

        BOOST_TEST(job->error() == KIO::ERR_CANNOT_ENTER_DIRECTORY);
    }

BOOST_AUTO_TEST_SUITE_END() // listDir_
BOOST_AUTO_TEST_SUITE_END() // kio_rar_
