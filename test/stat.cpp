#include "utility.hpp"
#include "uds_entry.hpp"

#include <memory>

#include <boost/test/unit_test.hpp>

#include <QDateTime>
#include <QDebug>
#include <QJsonArray>
#include <QJsonObject>
#include <QUrl>

#include <KIO/UDSEntry>
#include <KIO/StatJob>

BOOST_AUTO_TEST_SUITE(kio_rar_)
BOOST_AUTO_TEST_SUITE(stat_)

    void
    statAndCheck(QUrl const& url, QJsonObject const& data)
    {
        std::unique_ptr<KIO::StatJob> job {
            KIO::stat(
                url,
                KIO::StatJob::SourceSide,
                2,
                KIO::HideProgressInfo
            )
        };

        BOOST_REQUIRE(job->exec());

        auto const& entry = job->statResult();

        checkUDSEntry(entry, data);
    }

    BOOST_AUTO_TEST_CASE(ok1_)
    {
        auto const& data = makeJsonData(R"([
            {
                "type": "file",
                "name": "file1",
                "permission": "0100644",
                "date_time": "2019-03-21 15:02:02"
            }
        ])");

        QString const filename { "stat1.rar" };
        auto const& url = makeUrl(filename + "/file1");

        auto const& obj = data[0].toObject();

        statAndCheck(url, obj);
    }

    BOOST_AUTO_TEST_CASE(error1_)
    {
        QString const filename { "stat1.rar" };
        auto const& url = makeUrl(filename + "/file3");

        std::unique_ptr<KIO::StatJob> job {
            KIO::stat(url, KIO::HideProgressInfo)
        };

        BOOST_REQUIRE(!job->exec());

        BOOST_TEST(job->error() == KIO::ERR_DOES_NOT_EXIST);
    }

    BOOST_AUTO_TEST_CASE(error2_)
    {
        QString const filename { "test1x.rar" };
        auto const& url = makeUrl(filename);

        std::unique_ptr<KIO::StatJob> job {
            KIO::stat(url, KIO::HideProgressInfo)
        };

        BOOST_REQUIRE(!job->exec());

        BOOST_TEST(job->error() == KIO::ERR_DOES_NOT_EXIST);
    }

BOOST_AUTO_TEST_SUITE_END() // stat_
BOOST_AUTO_TEST_SUITE_END() // kio_rar_
