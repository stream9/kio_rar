## Introduction

KIO slave for RAR archive file.   
You can use it with KDE file managers (dolphin, etc...)

## Requirement

- C++17 compiler
- cmake
- KDE Frameworks 5
- boost library
- [unrar library](https://www.rarlab.com/rar_add.htm)
    - [libunrar (ex. for Arch linux)](https://www.archlinux.org/packages/extra/x86_64/libunrar/)

## Note

- Password protected archive is not supported
- get(), listDir() operation doesn't support symbolic link entry in the archive.