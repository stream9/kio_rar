add_library(kio_rar MODULE
    main.cpp
    debug.cpp
    slave.cpp
)

target_compile_features(kio_rar PRIVATE cxx_std_17)

target_compile_options(kio_rar PRIVATE
    -Wall -Wextra -pedantic -Wconversion -Wsign-conversion
)

target_link_libraries(kio_rar PRIVATE
    KF5::I18n
    KF5::KIOCore
    unrar
)

set_target_properties(kio_rar PROPERTIES
    CXX_VISIBILITY_PRESET "hidden"
    VERSION   "1.0"
    SOVERSION "1.0"
    LIBRARY_OUTPUT_NAME "rar"
)

install(TARGETS kio_rar DESTINATION ${KDE_INSTALL_PLUGINDIR}/kf5/kio)
