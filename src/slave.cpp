#include "slave.hpp"

#include "debug.hpp"

#include <cassert>
#include <memory>
#include <utility>

#include <QByteArray>
#include <QDateTime>
#include <QFileInfo>
#include <QMimeDatabase>
#include <QString>
#include <QUrl>

#include <KIO/UDSEntry>
#include <KLocalizedString>

#define _UNIX
#include <unrar/dll.hpp>

namespace kio_rar {

constexpr int ERR_FILE_ACCESS = -1;
constexpr int ERR_SYMLINK = -2;

class Handle
{
public:
    Handle(QString const filename, UINT const mode)
    {
        ::RAROpenArchiveData param {};

        auto const& buf = filename.toLocal8Bit();

        param.ArcName = const_cast<char*>(buf.constData());
        param.OpenMode = mode;

        m_handle = ::RAROpenArchive(&param);
    }

    operator bool() const { return m_handle != nullptr; }
    operator HANDLE() const { return m_handle; }

    ~Handle() noexcept
    {
        if (m_handle != nullptr && ::RARCloseArchive(m_handle) != 0) {
            qCDebug(KIO_RAR_LOG) << "file close error\n";
        }
    }

private:
    HANDLE m_handle = nullptr;
};

// https://docs.microsoft.com/en-us/windows/desktop/api/winbase/nf-winbase-dosdatetimetofiletime
static QDateTime
fromMsDosDateTime(uint32_t const v)
{
    auto const year   = static_cast<int>((v >> 25) + 1980);
    auto const month  = static_cast<int>((v >> 21) & 0b1111);
    auto const day    = static_cast<int>((v >> 16) & 0b11111);
    auto const hour   = static_cast<int>((v >> 11) & 0b11111);
    auto const minute = static_cast<int>((v >> 5) & 0b111111);
    auto const second = static_cast<int>((v & 0b11111) * 2);

    QDate date { year, month, day };
    QTime time { hour, minute, second };

    return { date, time };
}

static auto
makeEntry(::RARHeaderDataEx const& header)
{
    using KIO::UDSEntry;

    UDSEntry e;

    auto const& path = QString::fromLocal8Bit(header.FileName);

    auto const idx = path.lastIndexOf('/');
    auto const& name = idx == -1 ? path : path.mid(idx + 1);

    e.fastInsert(UDSEntry::UDS_NAME, name);
    e.fastInsert(UDSEntry::UDS_FILE_TYPE, header.FileAttr);
    e.fastInsert(UDSEntry::UDS_SIZE, header.UnpSize);
    e.fastInsert(UDSEntry::UDS_ACCESS, header.FileAttr);
    e.fastInsert(UDSEntry::UDS_MODIFICATION_TIME,
                 fromMsDosDateTime(header.FileTime).toTime_t());
#if 0
    e.fastInsert(UDSEntry::UDS_LINK_DEST, entry.symLinkTarget());
#endif

    return e;
}

// @return { file_name, entry_name }
// @return {} when there in't a file in the path
static std::pair<QString, QString>
splitPath(QString const& path)
{
    if (path.isEmpty() || path == "/") return {};

    // assert(path[0] == '/')

    int idx = path.indexOf('/', 1);

    while (true) {
        auto const& prefix = path.left(idx);
        QFileInfo f { prefix };

        if (!f.exists()) {
            break;
        }
        else if (f.isFile() || f.isSymLink()) {
            QString const& entryPath = idx == -1 ? "/" : path.mid(idx);

            return std::make_pair(prefix, entryPath);
        }

        if (idx == -1) break;
        idx = path.indexOf('/', idx + 1);
    };

    return {};
}

static void
normalizeEntryPath(QString& path)
{
    // remove leading '/'
    if (!path.isEmpty() && path.front() == '/') {
        path = path.mid(1);
    }

    // remove trailing '/'
    if (!path.isEmpty() && path.back() == '/') {
        path = path.left(-1);
    }
}

struct CallbackContext {
    Slave& slave;
    QString path;
    KIO::filesize_t processed;
    bool first;
};

static int
callback(UINT const msg, LPARAM const userData,
         LPARAM const p1, LPARAM const p2)
{
    if (msg == UCM_PROCESSDATA) {
        auto* const context = reinterpret_cast<CallbackContext*>(userData);
        auto* const ptr = reinterpret_cast<char const*>(p1);
        auto const size = static_cast<int>(p2); // p2 <= 4 * 1024 * 1024

        assert(context);
        assert(ptr);

        QByteArray chunk { ptr, size };

        if (context->first) {
            QMimeDatabase db;
            auto const& mime =
                db.mimeTypeForFileNameAndData(context->path, chunk);

            context->slave.mimeType(mime.name());
            context->first = false;
        }

        context->slave.data(chunk);

        context->processed += static_cast<KIO::filesize_t>(size);
        context->slave.processedSize(context->processed);

        return 1;
    }
    else if (msg == UCM_CHANGEVOLUME) {
        //TODO
    }
    else { // msg == UCM_NEEDPASSWORD
        //TODO
    }

    return 0;
}

//
// Slave
//
Slave::
Slave(QByteArray const& proto,
      QByteArray const& pool, QByteArray const& app)
    : KIO::SlaveBase { proto, pool, app }
{}

Slave::~Slave() = default;

void Slave::
listDir(QUrl const& url)
{
    qCDebug(KIO_RAR_LOG) << __func__ << url;
    auto [filePath, entryPath] = splitPath(url.path());

    if (filePath.isEmpty()) {
        emitError(KIO::ERR_CANNOT_ENTER_DIRECTORY, url);
        return;
    }

    normalizeEntryPath(entryPath);

    if (auto err = loadArchive(filePath); err != KJob::NoError) {
        emitError(err, url);
        return;
    }

    auto* const entry = findEntry(entryPath);
    if (!entry) {
        emitError(KIO::ERR_CANNOT_ENTER_DIRECTORY, url);
        return;
    }
    else if (!entry->isDir()) {
        emitError(KIO::ERR_IS_FILE, url);
        return;
    }

    KIO::UDSEntryList entries;

    entries.push_back(m_currentDir);

    auto match = [](auto& filename, auto& path) {
        if (filename.isEmpty()) return false;

        if (path.isEmpty()) {
            return !filename.contains('/');
        }

        if (!filename.startsWith(path)) {
            return false;
        }
        else {
            auto const& rest = filename.midRef(path.size());
            return !rest.contains('/');
        }
    };

    if (!entryPath.isEmpty() && entryPath.back() != '/') {
        entryPath.push_back('/');
    }

    for (auto const& e: m_entries) {
        if (match(e.path, entryPath)) {
            entries.push_back(e.entry);
        }
    }

    this->totalSize(static_cast<KIO::filesize_t>(entries.size()));
    this->listEntries(entries);

    this->finished();
}

void Slave::
stat(QUrl const& url)
{
    qCDebug(KIO_RAR_LOG) << __func__ << url;
    auto [filePath, entryPath] = splitPath(url.path());

    normalizeEntryPath(entryPath);

    if (filePath.isEmpty()) {
        emitError(KIO::ERR_DOES_NOT_EXIST, url);
        return;
    }

    if (auto err = loadArchive(filePath); err != KJob::NoError) {
        emitError(err, url);
        return;
    }

    auto* const entry = findEntry(entryPath);

    if (entry) {
        this->statEntry(*entry);
        this->finished();
    }
    else {
        emitError(KIO::ERR_DOES_NOT_EXIST, url);
    }
}

void Slave::
get(QUrl const& url)
{
    qCDebug(KIO_RAR_LOG) << __func__ << url;
    auto [filePath, entryPath] = splitPath(url.path());

    normalizeEntryPath(entryPath);

    if (filePath.isEmpty()) {
        emitError(KIO::ERR_DOES_NOT_EXIST, url);
        return;
    }

    if (!QFileInfo::exists(filePath)) {
        emitError(KIO::ERR_DOES_NOT_EXIST, url);
        return;
    }

    Handle handle { filePath, RAR_OM_EXTRACT };
    if (!handle) {
        emitError(KIO::ERR_CANNOT_OPEN_FOR_READING, url);
        return;
    }

    while (true) {
        ::RARHeaderDataEx header {};

        auto rv = ::RARReadHeaderEx(handle, &header);
        if (rv == ERAR_END_ARCHIVE) {
            break;
        }
        else if (rv == ERAR_BAD_DATA) {
            emitError(ERR_FILE_ACCESS, url);
            return;
        }

        auto const& path = QString::fromLocal8Bit(header.FileName);

        if (path == entryPath) {
            if (S_ISDIR(header.FileAttr)) {
                emitError(KIO::ERR_IS_DIRECTORY, url);
                return;
            }
            else if (S_ISLNK(header.FileAttr)) {
                emitError(ERR_SYMLINK, url);
                return;
            }

            this->totalSize(static_cast<KIO::filesize_t>(header.UnpSize));

            CallbackContext context { *this, path, 0, true };

            ::RARSetCallback(
                handle, callback, reinterpret_cast<LPARAM>(&context));

            rv = ::RARProcessFile(handle, RAR_TEST, nullptr, nullptr);

            if (rv != 0) {
                emitError(ERR_FILE_ACCESS, url);
                return;
            }

            this->finished();
            return;
        }
        else {
            rv = ::RARProcessFile(handle, RAR_SKIP, nullptr, nullptr);

            if (rv != 0) {
                emitError(ERR_FILE_ACCESS, url);
                return;
            }
        }
    }

    emitError(KIO::ERR_DOES_NOT_EXIST, url);
}

int Slave::
loadArchive(QString const& filePath)
{
    QFileInfo info { filePath };

    if (!info.exists()) {
        return KIO::ERR_DOES_NOT_EXIST;
    }

    if (filePath == m_filePath && info.lastModified() == m_lastModified) {
        return KJob::NoError;
    }

    Handle handle { filePath, RAR_OM_LIST };
    if (!handle) {
        return KIO::ERR_CANNOT_OPEN_FOR_READING;
    }

    m_filePath = filePath;
    m_lastModified = info.lastModified();
    m_entries.clear();

    m_currentDir.clear();
    m_currentDir.fastInsert(KIO::UDSEntry::UDS_NAME, "." );
    m_currentDir.fastInsert(KIO::UDSEntry::UDS_FILE_TYPE, S_IFDIR );
    m_currentDir.fastInsert(KIO::UDSEntry::UDS_MODIFICATION_TIME,
                            m_lastModified.currentSecsSinceEpoch());

    while (true) {
        ::RARHeaderDataEx header {};

        auto rv = ::RARReadHeaderEx(handle, &header);
        if (rv == ERAR_END_ARCHIVE) {
            break;
        }
        else if (rv == ERAR_BAD_DATA) {
            return ERR_FILE_ACCESS;
        }

        m_entries.push_back({
            QString::fromLocal8Bit(header.FileName),
            makeEntry(header)
        });

        rv = ::RARProcessFile(handle, RAR_SKIP, nullptr, nullptr);
        if (rv != 0) {
            return ERR_FILE_ACCESS;
        }
    }

    return KJob::NoError;
}

KIO::UDSEntry const* Slave::
findEntry(QString const& path) const
{
    if (path.isEmpty()) {
        return &m_currentDir;
    }
    else {
        auto const it = std::find_if(m_entries.begin(), m_entries.end(),
            [&](auto& e) {
                return e.path == path;
            });

        if (it != m_entries.end()) {
            return &it->entry;
        }
        else {
            return nullptr;
        }
    }
}

void Slave::
emitError(int const error, QUrl const& url)
{
    switch (error) {
        case ERR_FILE_ACCESS:
            this->error(KIO::ERR_SLAVE_DEFINED,
               i18n("File access error: %1", url.toDisplayString()) );
            break;
        case ERR_SYMLINK:
            this->error(KIO::ERR_SLAVE_DEFINED,
               i18n("Symlink isn't supported: %1", url.toDisplayString()) );
            break;
        default:
            this->error(error, url.toDisplayString());
            break;
    }
}

} // namespace kio_rar
