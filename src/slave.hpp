#ifndef KIO_RAR_SLAVE_HPP
#define KIO_RAR_SLAVE_HPP

#include <vector>

#include <QString>
#include <QDateTime>

#include <KIO/SlaveBase>
#include <KIO/UDSEntry>

class QByteArray;
class QUrl;

namespace kio_rar {

struct Entry {
    QString path;
    KIO::UDSEntry entry;
};

class Slave : public KIO::SlaveBase
{
public:
    Slave(QByteArray const& proto,
          QByteArray const& pool, QByteArray const& app);

    ~Slave() override;

    void listDir(QUrl const& url) override;
    void stat(QUrl const& url) override;
    void get(QUrl const& url) override;

private:
    int loadArchive(QString const& filePath);

    KIO::UDSEntry const*
        findEntry(QString const& entryPath) const;

    void emitError(int error, QUrl const&);

private:
    QString m_filePath;
    QDateTime m_lastModified;
    KIO::UDSEntry m_currentDir;
    std::vector<Entry> m_entries;
};

} // namespace kio_rar

#endif // KIO_RAR_SLAVE_HPP
