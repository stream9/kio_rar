#include "slave.hpp"

#include <cstdio>

#include <QCoreApplication>
#include <QObject>

class KIOPluginForMetaData : public QObject
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.kde.kio.slave.rar" FILE "metadata.json")
};

extern "C" { int Q_DECL_EXPORT kdemain(int argc, char* argv[]); }

int
kdemain(int argc, char* argv[])
{
    if (argc != 4) {
        std::fprintf(stderr,
            "Usage: %s protocol domain-socket1 domain-socket2\n",
            argv[0]
        );

        return -1;
    }

    QCoreApplication app { argc, argv };

    kio_rar::Slave slave { argv[1], argv[2], argv[3] };
    slave.dispatchLoop();

    return 0;
}

#include "main.moc"

